var React = require('react');
var ReactPivot = require('react-pivot');
var createReactClass = require('create-react-class');

var rows = require('./data.json');

var dimensions = [{value: "date", title: "Date"}, {value: "host", title: "Host"}];

var reduce = function (row, data) {
    data.impressions = (data.impressions || 0);
    data.impressions = row.type === 'impression' ? data.impressions + 1 : data.impressions;
    data.loads = (data.loads || 0);
    data.loads = row.type === 'load' ? data.loads + 1 : data.loads;
    data.displays = (data.displays || 0);
    data.displays = row.type === 'display' ? data.displays + 1 : data.displays;
    return data;
};

var calculations = [
    {
        title: "Impressions", value: 'impressions'
    },
    {
        title: 'Loads', value: 'loads'
    },
    {
        title: 'Displays', value: 'displays'
    },
    {
        title: "Load Rate",
        value: function (row) {
            return (row.loads / row.impressions) * 100;
        },
        template: function (val) {
            return val.toFixed(2) + "%"
        }
    },
    {
        title: "Display Rate",
        value: function (row) {
            return (row.displays / row.loads) * 100;
        },
        template: function (val) {
            return val.toFixed(2) + "%"
        }
    }
];

module.exports = createReactClass({
    render () {
        return (<ReactPivot rows={rows}
                            dimensions={dimensions}
                            reduce={reduce}
                            calculations={calculations}
                            activeDimensions={['Date', "Host"]}/>)
    }
});
